#include "image.h"
#include <stdint.h>
#include <stdio.h>

enum read_status {
  READ_OK = 0,
  READ_ERROR,
  READ_INVALID_SIGNATURE,
  READ_MALLOC_ERROR,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
};

struct image malloc_image(uint32_t width, uint32_t height);

enum read_status from_bmp(FILE *in, struct image *img);

enum write_status { WRITE_OK = 0, WRITE_ERROR };

enum write_status to_bmp(FILE *out, const struct image *img);
