#include <image.h>
#include <stdint.h>
#pragma once

struct image rotate(struct image source, int64_t angle);
