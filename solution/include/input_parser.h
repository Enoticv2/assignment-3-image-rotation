#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#pragma once

struct input_values {
  bool is_valid;
  char *input_file;
  char *output_file;
  int64_t angle;
};

static const struct input_values wrong_input = {false, NULL, NULL, 0};

struct input_values parse_input(int argc, char **argv);
