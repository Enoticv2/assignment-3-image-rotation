#include "input_parser.h"

struct input_values parse_input(int argc, char **argv) {
  if (argc != 4) {
    return wrong_input;
  }
  return (struct input_values){true, argv[1], argv[2], atoi(argv[3])};
}
