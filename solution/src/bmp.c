#include "bmp.h"
#include "image.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define BMP_BF_TYPE 0x4D42
#define BMP_BF_RESERVED 0
#define BMP_BI_PLANES 1
#define BMP_BI_BITCOUNT 24
#define BMP_BI_COMPRESSION 0
#define BMP_BI_XPEELSPERMETER 2834
#define BMP_BI_YPEELSPERMETER 2834
#define BMP_BI_CLRUSED 0
#define BMP_BI_CLRIMPORTANT 0

struct image malloc_image(uint32_t width, uint32_t height) {
  struct image image = {.width = width, .height = height, .data = NULL};
  image.data = malloc(width * height * sizeof(struct pixel));
  return image;
}

uint8_t calculate_padding(uint32_t width) {
  if (width % 4 == 0) {
    return 0;
  }
  return 4 - width * sizeof(struct pixel) % 4;
}

enum read_status from_bmp(FILE *in, struct image *img) {
  struct bmp_header bmp_header;
  if (fread(&bmp_header, sizeof(struct bmp_header), 1, in) != 1) {
    return READ_INVALID_BITS;
  }
  *img = malloc_image(bmp_header.biWidth, bmp_header.biHeight);
  if (!img->data) {
    fprintf(stderr, "Cannot allocate memory!\n");
    return READ_INVALID_BITS;
  }
  fseek(in, bmp_header.bOffBits, SEEK_SET);

  uint8_t padding = calculate_padding(bmp_header.biWidth);

  for (uint64_t i = 0; i < bmp_header.biHeight; i++) {
    fread(img->data + i * bmp_header.biWidth, sizeof(struct pixel),
          bmp_header.biWidth, in);
    fseek(in, padding, SEEK_CUR);
  }
  return READ_OK;
}
struct bmp_header create_header(const struct image *img) {
  return (struct bmp_header){
      .bfType = BMP_BF_TYPE,
      .bfileSize = img->height * (calculate_padding(img->width) +
                                  img->width * sizeof(struct pixel)),
      .bfReserved = BMP_BF_RESERVED,
      .bOffBits = sizeof(struct bmp_header),
      .biSize = sizeof(struct bmp_header),
      .biWidth = img->width,
      .biHeight = img->height,
      .biPlanes = BMP_BI_PLANES,
      .biBitCount = BMP_BI_BITCOUNT,
      .biCompression = BMP_BI_COMPRESSION,
      .biSizeImage = sizeof(struct bmp_header) +
                     img->height * (calculate_padding(img->width) +
                                    img->width * sizeof(struct pixel)),
      .biXPelsPerMeter = BMP_BI_XPEELSPERMETER,
      .biYPelsPerMeter = BMP_BI_YPEELSPERMETER,
      .biClrUsed = BMP_BI_CLRUSED,
      .biClrImportant = BMP_BI_CLRIMPORTANT};
}
enum write_status to_bmp(FILE *out, const struct image *img) {
  struct bmp_header header = create_header(img);
  if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) {
    fprintf(stderr, "Cannot read header!\n");
    return WRITE_ERROR;
  }
  uint8_t padding = calculate_padding(img->width);
  for (uint32_t i = 0; i < img->height; i++) {
    fwrite(img->data + img->width * i, sizeof(struct pixel), img->width, out);
    fseek(out, padding, SEEK_CUR);
  }

  return WRITE_OK;
}
