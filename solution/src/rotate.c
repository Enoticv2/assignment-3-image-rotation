#include "bmp.h"
#include "image.h"
#include <inttypes.h>
#include <rotate.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

static struct image rotate_90(const struct image source) {
  struct image output_image = malloc_image(source.height, source.width);
  if (!output_image.data) {
    return output_image;
  }
  for (uint32_t i = 0; i < output_image.height; i++) {
    for (uint32_t j = 0; j < output_image.width; j++) {
      output_image.data[output_image.width * i + j] =
          source.data[source.width * (j + 1) - 1 - i];
    }
  }
  return output_image;
}

struct image rotate(struct image source, int64_t angle) {
  if (angle < 0) {
    angle += 360;
  }
  uint8_t rotate_count = angle / 90;
  struct image output_image = source;
  for (long i = 0; i < rotate_count; i++) {
    output_image = rotate_90(source);
    free(source.data);
    source = output_image;
  }
  return output_image;
}
